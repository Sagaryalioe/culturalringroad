from django.contrib import admin

from .models import Place, Gallery


class PlaceModelAdmin(admin.ModelAdmin):
    list_display = ["__str__", "index"]

    class Meta:
        model = "Place"


class GalleryModelAdmin(admin.ModelAdmin):
    list_display = ["title", "video_url", "active"]
    list_editable = ["active"]

    class Meta:
        model = "Gallery"


admin.site.register(Place, PlaceModelAdmin)
admin.site.register(Gallery, GalleryModelAdmin)
