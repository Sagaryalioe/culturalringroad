from django.shortcuts import render
from .models import Place
# Create your views here.


def place(request):
    places = Place.objects.all()
    context = {
        "places": places,
        "place_active": True
    }
    return render(request, 'place/place.html', context)


def place_detail(request, slug=None):
    instance = Place.objects.get(slug=slug)

    context = {
        "instance": instance,
        "place_active": True
    }

    return render(request, 'place/detail.html', context)
