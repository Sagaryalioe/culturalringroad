from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField
from django.urls import reverse


class Place(models.Model):
    index = models.IntegerField(unique=True)
    name = models.CharField(max_length=250)
    slug = models.SlugField(
        unique=True, help_text="Please Enter a text in all small letters and use '-' instead of spaces if required.")
    image = models.ImageField(upload_to='places/')
    short_description = models.TextField()
    long_description = RichTextUploadingField(default=None)
    long_description_nepali = RichTextUploadingField(
        default=None, blank=True, null=True)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse("place:placeDetail", kwargs={"slug": self.slug})

    class Meta:
        ordering = ["index"]
        verbose_name = "Place"
        verbose_name_plural = "Places"


class Gallery(models.Model):
    title = models.CharField(max_length=350)
    video_url = models.URLField(
        max_length=200, help_text="Place Youtube embed url")
    active = models.BooleanField(default=True)
    uploaded_at = models.DateTimeField(auto_now=True, auto_now_add=False)

    def __str__(self):
        return self.title
