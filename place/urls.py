from . import views
from django.urls import path

app_name = "place"

urlpatterns = [
    path('', views.place, name="place"),
    path('<slug:slug>', views.place_detail, name="placeDetail")
]
