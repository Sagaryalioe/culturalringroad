#!/bin/sh

# Variables passed from .gitlab-ci.yml
# $1: RELEASE_IMAGE 
# $2: IMAGE_NAME

# any future command that fails will exit the script
set -e

mkdir -p ~/.ssh/
# Lets write the public key of our aws instance or any other server
echo -e "$PRIVATE_KEY" > ~/.ssh/id_rsa
chmod 600 ~/.ssh/id_rsa

# ** Alternative approach
# # eval $(ssh-agent -s)
# # echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
# ** End of alternative approach

# disable the host key checking.
mkdir -p ~/.ssh
touch ~/.ssh/config
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config

ssh techkunja@$DEPLOY_SERVER 'bash -s' < ./deploy/serverTasks.sh $1 $2