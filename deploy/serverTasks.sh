#!/bin/sh

# Variables passed from .gitlab-ci.yml
# $1: RELEASE_IMAGE 
# $2: IMAGE_NAME

set -e

which docker || ( sudo apt update -y && sudo apt install apt-transport-https ca-certificates curl software-properties-common -y && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - && sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable" -y && sudo apt update -y && sudo apt-cache policy docker-ce && sudo apt install docker-ce )

sudo docker version

NC='\033[0m' # No Color
RED='\033[1;31m'
GREEN='\033[1;32m'
BLUE='\033[1;36m'
YELLOW='\033[1;33m'

ENV_FILE=/home/techkunja/.env

MEDIA_DIR=`cat $ENV_FILE | grep -w MEDIA_DIR | cut -d '=' -f2`
PORT=`cat $ENV_FILE | grep -w PORT | cut -d '=' -f2`

echo -e "${YELLOW}Pulling the latest image - ${BLUE}$1${YELLOW} from the docker hub ...${NC}"
sudo docker pull $1

dockerStatus=`sudo docker ps --format "{{.Names}}" --filter "name=$2"`


# means dockerStatus is empty
if [ -z "$dockerStatus" ]; then
    echo -e "${BLUE}$1 - $2 ${YELLOW}has not yet been started.${NC}"
    sudo docker run -d -p ${PORT}:8000 -v /var/run/postgresql:/postgresql_socket -v $MEDIA_DIR:/backend/media --env-file=$ENV_FILE --name="$2" --restart always $1
    echo -e "${GREEN}$1 - $2 running successfully on ${BLUE}PORT: ${PORT}${NC}"
else
    echo -e "${BLUE}$1 - $2 ${YELLOW}is already up and running.${NC}"
    echo -e "${RED}Shutting down the previous container.${NC}"
    sudo docker stop "$2"
    echo -e "${RED}Removing the previous container.${NC}"
    sudo docker container rm "$2"
    echo -e "${GREEN}Starting the latest pulled image ...${NC}"
    sudo docker run -d -p ${PORT}:8000 -v /var/run/postgresql:/postgresql_socket -v $MEDIA_DIR:/backend/media --env-file=$ENV_FILE --name="$2" --restart always $1
    echo -e "${GREEN}$1 - $2 running successfully on ${BLUE}PORT: ${PORT}${NC}"
fi