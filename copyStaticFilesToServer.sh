#!/bin/bash

############################################################
# THIS FILE IS SOLEY TO BE USED FROM LOCAL MACHINE.        #
# AND HAS NO WHATSOEVER USE/LINK IN DOCKER OR GITLAB-CI/CD #
############################################################

#################################################
# Run this file as:
# ./copyStaticFilesToServer.sh
# in the terminal to copy the local static files
# to the remote server
#################################################

# Get directory location in the server to where static files are to be copied
STATIC_DIR=`ssh techkunja@techkunja.com.np "cat /home/techkunja/.env | grep -w STATIC_DIR | cut -d '=' -f2"`

rsync -rvuzh --progress --del static/ techkunja@techkunja.com.np:$STATIC_DIR --rsync-path="sudo rsync"
