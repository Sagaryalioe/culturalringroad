# Generated by Django 2.2.1 on 2019-05-28 03:20

import ckeditor_uploader.fields
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('home', '0004_delete_place'),
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(max_length=200)),
                ('phone_number1', models.CharField(max_length=10, verbose_name='Primary Phone number')),
                ('phone_number2', models.CharField(blank=True, max_length=10, null=True, verbose_name='Secondary Phone number')),
                ('email', models.EmailField(max_length=254)),
            ],
        ),
        migrations.AddField(
            model_name='about',
            name='long_description_nepali',
            field=ckeditor_uploader.fields.RichTextUploadingField(default=None),
        ),
    ]
