from home.models import About, Contact


def common(request):
    about = About.objects.all()
    contact = Contact.objects.all()
    return {
        'about': about and about[0],
        'contact': contact and contact[0],
    }
