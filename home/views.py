from django.shortcuts import render
from .models import Slider, About
from place.models import Place, Gallery


def coming_soon(request):
    launchDate = "2019/05/17"
    context = {
        "launchDate": launchDate
    }
    return render(request, 'home/coming-soon.html', context)


def index(request):
    slider_list = Slider.objects.all()
    places = Place.objects.all()
    # about = About.objects.all()
    context = {
        "slider_list": slider_list,
        "places": places,
        # "about": about and about[0],
        "home_active": True
    }
    return render(request, 'home/index.html', context)


def gallery(request):
    gallery = Gallery.objects.filter(active=True)
    context = {
        'gallery': gallery,
        'gallery_active': True
    }
    return render(request, 'home/gallery.html', context)
