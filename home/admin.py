from django.contrib import admin
from .models import Slider, Social, About, Contact
# Register your models here.


class SliderModelAdmin(admin.ModelAdmin):
    list_display = ["__str__"]

    class Meta:
        model = "Slider"


class AboutModelAdmin(admin.ModelAdmin):
    list_display = ["__str__"]

    class Meta:
        model = "About"


class SocialModelAdmin(admin.ModelAdmin):
    list_display = ["__str__"]

    class Meta:
        model = "Social"


class ContactModelAdmin(admin.ModelAdmin):
    list_display = ["__str__", "phone_number1", "phone_number2", "email"]

    class Meta:
        model = "Contact"


admin.site.site_header = 'Cultural Ring Road Admin Panel'
admin.site.register(Slider, SliderModelAdmin)
admin.site.register(About, AboutModelAdmin)
admin.site.register(Social, SocialModelAdmin)
admin.site.register(Contact, ContactModelAdmin)
