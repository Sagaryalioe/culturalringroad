from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField

# Create your models here.


class Slider(models.Model):
    image = models.ImageField(upload_to='slider/')
    caption = models.CharField(max_length=250)
    active = models.BooleanField(default=True)

    class Meta:
        verbose_name = "slider"
        verbose_name_plural = "sliders"

    def __str__(self):
        return self.caption


class About(models.Model):
    short_content = models.TextField()
    long_description = RichTextUploadingField(default=None)
    long_description_nepali = RichTextUploadingField(default=None)

    def __str__(self):
        return self.short_content


class Social(models.Model):
    facebook = models.URLField(
        "Facebook Page", max_length=200, blank=True, null=True)
    youtube = models.URLField(
        "Youtube Channel", max_length=200, blank=True, null=True)

    def __str__(self):
        return self.facebook


class Contact(models.Model):
    address = models.CharField(max_length=200)
    phone_number1 = models.CharField("Primary Phone number", max_length=10)
    phone_number2 = models.CharField(
        "Secondary Phone number", max_length=10, blank=True, null=True)
    email = models.EmailField(max_length=254)

    def __str__(self):
        return self.address
