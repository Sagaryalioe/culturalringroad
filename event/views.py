from django.shortcuts import render
from .models import Event
# Create your views here.


def event(request):
    events = Event.objects.filter(active=True)
    running_events = Event.objects.filter(active=True, running=True)
    not_running_events = Event.objects.filter(active=True, running=False)
    context = {
        'events': events,
        'running_events': running_events,
        'not_running_events': not_running_events,
        "event_active": True
    }
    return render(request, 'event/events.html', context)


def event_detail(request, slug=None):
    instance = Event.objects.get(slug=slug)
    context = {
        "instance": instance,
        "event_active": True
    }
    return render(request, 'event/event_detail.html', context)
