from django.contrib import admin
from .models import Event


class EventModelAdmin(admin.ModelAdmin):
    list_display = ['name', 'duration', 'start_date', 'active', 'running']
    list_filter = ['active', 'running']
    list_editable = ['active', 'running']

    class Meta:
        model = Event


admin.site.register(Event, EventModelAdmin)
