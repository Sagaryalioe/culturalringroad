from . import views
from django.urls import path

app_name = "event"

urlpatterns = [
    path('', views.event, name="event"),
    path('<slug:slug>/', views.event_detail, name="event_detail")
]
