from django.db import models
from place.models import Place
from ckeditor_uploader.fields import RichTextUploadingField
from django.urls import reverse


class Event(models.Model):
    name = models.CharField(max_length=250)
    slug = models.SlugField()
    image = models.ImageField(upload_to='Event/')
    place = models.ManyToManyField(Place, related_name="events")
    duration = models.IntegerField(help_text="duration of event in Days")
    start_date = models.DateField()
    short_description = models.TextField()
    long_description = RichTextUploadingField(default=None)
    active = models.BooleanField(default=False)
    running = models.BooleanField(default=False)

    def __str__(self):
        return self.name

    def get_locations(self):
        pass

    def get_absolute_url(self):
        return reverse("event:event_detail", kwargs={"slug": self.slug})
